import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataProvider {

  items: Array<{ title: string }>;

  constructor(private http: Http) {

    this.items = [
      { title: 'one' },
      { title: 'two' },
      { title: 'three' },
      { title: 'four' },
      { title: 'five' },
      { title: 'six' },
      { title: 'seven' },
      { title: 'eight' },
      { title: 'nine' },
    ];

  }

  filteredItems = (searchTerm: string): Array<{ title: string }> => {
    return this.items.filter(item => {
      const st = searchTerm.toLocaleLowerCase();
      return item.title.indexOf(st) >= 0;
    });
  };

}

