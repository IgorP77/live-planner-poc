import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@Component({
  templateUrl: 'build/pages/home/home.html',
  providers: [DataProvider]
})
export class HomePage {

  searchTerm: string = '';
  items: Array<{title: string}>;

  constructor(public navCtrl: NavController, private dataService: DataProvider) {

  }

  ionViewLoaded() {
    this.setFilteredItems();
  }

  private setFilteredItems() {
    this.items = this.dataService.filteredItems(this.searchTerm);
  }
}
